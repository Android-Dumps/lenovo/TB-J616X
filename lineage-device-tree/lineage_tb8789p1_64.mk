#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Lineage stuff.
$(call inherit-product, vendor/lineage/config/common_full_phone.mk)

# Inherit from tb8789p1_64 device
$(call inherit-product, device/lenovo/tb8789p1_64/device.mk)

PRODUCT_DEVICE := tb8789p1_64
PRODUCT_NAME := lineage_tb8789p1_64
PRODUCT_BRAND := Lenovo
PRODUCT_MODEL := Lenovo TB-J616X
PRODUCT_MANUFACTURER := lenovo

PRODUCT_GMS_CLIENTID_BASE := android-lenovo

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="full_tb8789p1_64-user 12 SP1A.210812.016 TB-J616X_S240155_230210_ROW release-keys"

BUILD_FINGERPRINT := Lenovo/TB-J616X/TB-J616X:12/SP1A.210812.016/TB-J616X_S240155_230210_ROW:user/release-keys
